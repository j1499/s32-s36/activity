const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers")
const auth = require("../auth");

//Route for creating a course
// router.post("/", (req, res) => {

// 	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
// })


// Route for creating a course Activity s34 - Admin
router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if (userData.isAdmin) {
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
		return true
	} else {
		res.send(false)
	}
});


// Route for retrieving all the courses
router.get("/all", auth.verify, (req, res) => {

	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))

})


/*
	Mini Activity

	Create a route and controller for retrieving all active courses. No need to login 

	1. getAllActive()
	2. "/" endpoint

*/
// Route for retrieving all active courses - Mini Activity
router.get("/", (req, res) => {

	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
})


// Route for retrieving a specific course
// Url: localhost:4000/courses/62223b123b0f2de71c3ad160
// Request params = dynamic url
router.get("/:courseId", (req, res) => {

	console.log(req.params.courseId);
	console.log(req.params);

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));


})

// Route for updating course
router.put("/:courseId", auth.verify, (req, res) => {

	const data = {

		courseId: req.params.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		updatedCourse: req.body
	
	}

	console.log(data)

	courseController.updateCourse(data).then(resultFromController => res.send(resultFromController));


})

// Activity s35
router.put("/:courseId/archive", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData)

	if (userData.isAdmin) {
		courseController.archivedCourse(req.params).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("Need admin access to archive a course")
	}
})



module.exports = router;

