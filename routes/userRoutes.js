const express = require("express");
const router = express.Router();
// Connect controller to route
const userController = require("../controllers/userControllers");
const auth = require("../auth");

// Route for Checking if email exists
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for User Authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for details Activity s33
// router.post("/details", (req, res) => {
// 	userController.getProfile(req.body.id).then(resultFromController => res.send(resultFromController));
// })

// Discussion Route for User
// The "auth.verify" will act as a middleware to ensure that the user is logged in before they can get the details
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
});


// Route for enrolling a user
// router.post("/enroll", auth.verify, (req, res) => {

// 	let data = {
// 		userId: req.body.userId,
// 		courseId: req.body.courseId
// 	}

// 	userController.enroll(data).then(resultFromController => res.send(resultFromController))

// });

// Route for enrolling a user Activity s36
router.post("/enroll", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	let data = {
			userId: userData.id,
			courseId: req.body.courseId
		}

	if (userData.isAdmin !== true) {


		userController.enroll(data).then(resultFromController => res.send(resultFromController))
		

	} else {
		return res.send(`Only regular user can enroll`)
	}


})






module.exports = router;