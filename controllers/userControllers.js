const User = require("../models/User");

const Course = require("../models/Course");
//require bcrypt for hash
const bcrypt = require("bcrypt");

//require auth
const auth = require("../auth");

//Check if Email exists
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {

		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
}


// User Registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		//10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		//hashSync(<dataToBeHash>,<salt>)
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		if(error) {
			return false
		} else {
			return true
		}
	})
}

// User authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if (result == null){
			return false
		} else {
			//compareSync(dataToBeCompared, encryptedData)
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password)

			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

// User details Activity s33
// module.exports.getProfile = (userId) => {
	
// 	return User.findById(userId).then(result => {
		 
// 		 result.password = "";
		 
// 		 return result
// 	})
// }


// Discussion User details
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {
		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;
	});
};

// Enroll user to a class
// module.exports.enroll = async (data) => {

// 	console.log(data);

// 	let isUserUpdated = await User.findById(data.userId).then(user => {

// 		user.enrollments.push({courseId: data.courseId});

// 		//.then waiting for promise to save
// 		return user.save().then((user, error) => {

// 			if(error) {
// 				return false
// 			} else {
// 				return true
// 			}
// 		})
// 	})

// 	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

// 		course.enrollees.push({userId: data.userId});

// 		//.then waiting for promise to save
// 		return course.save().then((course, error) => {

// 			if(error) {
// 				return false
// 			} else {
// 				return true
// 			}

// 		})

// 	})


// 	if(isUserUpdated && isCourseUpdated) {
// 		return true
// 	} else {
// 		return false
// 	}


// }

// Enroll user to a class Activity s36
module.exports.enroll = async (data) => {

	console.log(data);

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId: data.courseId});

		//.then waiting for promise to save
		return user.save().then((user, error) => {

			if(error) {
				return false
			} else {
				return true
			}
		})
	})

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		course.enrollees.push({userId: data.userId});

		//.then waiting for promise to save
		return course.save().then((course, error) => {

			if(error) {
				return false
			} else {
				return true
			}

		})

	})


	if(isUserUpdated && isCourseUpdated) {
		return true
	} else {
		return false
	}


}




